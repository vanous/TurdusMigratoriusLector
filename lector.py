#!/usr/bin/env python3
import glob
import datetime
import re


def get_log(folder, file):
    path = f"{folder}{file}"
    f = open(path, "rb")
    content = f.read()
    f.seek(0xD)
    rdm = int.from_bytes(f.read(4), byteorder="little")
    start = content.rfind(b"\x50\x00\x00\x00")
    f.seek(start + 4)
    data_len = int.from_bytes(f.read(4), byteorder="little")
    # print(data_len)
    # f.read(2)
    data = f.read(data_len).decode("utf-8")
    return (data, rdm)


def parse_log(lines):
    print("start parsing: ", len(lines))
    data = []
    entry = {}
    entry["date"] = ""
    entry["logs"] = []
    version = None
    processors = False
    procs = []
    updated=""
    for l in lines.split("\n"):
        l = l.replace("\r", "")
        if is_version(l):
            if version is not None:  # is not first date
                data.append(entry)  # append previous data
                entry = {}
                entry["version"] = ""
                entry["date"] = ""
                entry["logs"] = []
                processors = False
            version = l
            entry["version"] = l
            if is_date_version(l):
                date = parse_date(l)
                if updated =="":
                    updated=int(date.timestamp())

                if updated < int(date.timestamp()):
                    updated=int(date.timestamp())

                entry["date"] = int(date.timestamp())
            else:
                entry["date"] = ""
            entry["logs"] = []
            processors = False
            continue

        d = (
            l.replace("------", "")
            .replace("\t- ", "")
            .replace("\t-", "")
            .replace('"', "'")
            .replace("&quot;", "'")
            .rstrip("\t")
        )

        if len(d) > 0:
            if processors:
                pr, sw = d.split("\t")
                sws = sw.split(",")
                procs.append({"proc": pr, "versions": sws})

            else:
                entry["logs"].append(d)
        if "------" in l:
            processors = True

    if version is not None:
        data.append(entry)
    return (data, procs, updated)


def is_version(data):
    m = re.compile("\d{8}|[V,v]\d{2,4}")
    return m.match(data)


def is_date_version(data):
    m = re.compile("\d{8}")
    return m.match(data)


def is_fresh(data, start):
    for l in data.split("\n"):
        l = l.replace("\r", "")
        if "-" not in l and len(l) == 8:
            # print(l)
            date = parse_date(l)

            if date.date() > start:
                return True

    return False


def fresh_lines(data, start):
    out = False
    for l in data.split("\n"):
        l = l.replace("\r", "")
        date = ""
        if "-" not in l and len(l) == 8:
            # print(l)
            date = parse_date(l)
            if date.date() > start:
                out = True
        if out:
            print(l.replace("------", ""), date)
        if "------" in l:
            out = False


def parse_date(data):
    hour = int(int(data[6:]) / 4)
    minute = (int(data[6:]) % 4) * 15
    # print(f"hour: {hour}, minute: {minute}")
    date = datetime.datetime.strptime(f"{data[:6]}{hour}{minute}", "%y%m%d%H%M")
    return date


def old_test():
    print("start")
    for lib in glob.glob("./files/*lib"):
        # print(lib)
        # print(get_log("", lib))
        data = get_log("", lib)
        start = datetime.date(2021, 6, 1)
        if is_fresh(data, start):
            print(lib)
            fresh_lines(data, start)
            print("\n")
