#!/usr/bin/env python3
import glob
import datetime
import lector
import json
import pprint


if __name__ == "__main__":
    print("start")
    all_data = []
    for lib in glob.glob("./files/*lib"):
        name = lib.split("/")[-1].replace(".lib", "")
        data, rdm = lector.get_log("", lib)
        logs, processors, updated = lector.parse_log(data)
        entry = {
            "rdm": f"{rdm:#06x}",
            "name": name,
            "changelogs": logs,
            "processors": processors,
            "updated": updated,
        }
        all_data.append(entry)

    pprint.pprint(all_data)
